import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:fowv/routes/config_route.dart';
import 'package:fowv/services/local_storage_service.dart';
import 'package:fowv/utils/constants.dart';
import 'package:get_it/get_it.dart';

class WebViewRoute extends StatefulWidget {
  final String initialURL;

  WebViewRoute({Key key, @required this.initialURL}) : super(key: key);

  @override
  _WebViewRouteState createState() => _WebViewRouteState();
}

class _WebViewRouteState extends State<WebViewRoute> {
  FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: const Text('To Be Defined'),
        actions: [
          IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: _goBack,
          ),
          IconButton(
            icon: Icon(
              Icons.refresh,
            ),
            onPressed: _refresh,
          ),
          IconButton(
            icon: Icon(Icons.clear),
            onPressed: _close,
          ),
        ],
      ),
      url: widget.initialURL,
    );
  }

  void _goBack() async {
    if (await flutterWebviewPlugin.canGoBack()) {
      flutterWebviewPlugin.goBack();
    }
  }

  void _refresh() {
    flutterWebviewPlugin.reload();
  }

  void _close() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (BuildContext context) => ConfigRoute()),
    );
    GetIt.I<LocalStorageService>().save(DB_PARAM, '');
  }
}

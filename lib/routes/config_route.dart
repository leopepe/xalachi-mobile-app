import 'package:flutter/material.dart';
import 'package:fowv/routes/webview_route.dart';
import 'package:fowv/services/local_storage_service.dart';
import 'package:get_it/get_it.dart';

import 'package:fowv/utils/constants.dart';

class ConfigRoute extends StatefulWidget {
  @override
  _ConfigRouteState createState() => _ConfigRouteState();
}

class _ConfigRouteState extends State<ConfigRoute> {
  final _textController = TextEditingController();
  var _isComposing = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('To Be Defined'),
      ),
      body: Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildLoginImage(),
              _buildTextEditor(),
              _buildLoginButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLoginImage() {
    return const Image(
      image: AssetImage('assets/login_image.png'),
      height: 200,
      width: 200,
    );
  }

  Widget _buildTextEditor() {
    return Container(
      width: 250,
      margin: const EdgeInsets.symmetric(vertical: 12.0),
      child: TextField(
        controller: _textController,
        onChanged: (String text) {
          setState(() {
            _isComposing = text.length > 0;
          });
        },
        onSubmitted: _isComposing ? _handleSubmitted : null,
        decoration: const InputDecoration(
          hintText: 'Hint text',
        ),
      ),
    );
  }

  Widget _buildLoginButton() {
    return RaisedButton(
      onPressed: () =>
          _isComposing ? _handleSubmitted(_textController.text) : null,
      child: const Text('Go!'),
    );
  }

  void _handleSubmitted(String param) {
    setState(() {
      _textController.clear();
      _isComposing = false;
    });
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                WebViewRoute(initialURL: BASE_URL + param)));
    GetIt.I<LocalStorageService>().save(DB_PARAM, param);
  }
}

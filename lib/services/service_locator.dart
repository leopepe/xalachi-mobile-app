import 'package:get_it/get_it.dart';

import 'local_storage_service.dart';

Future<void> setupLocator() async {
  final localStorageService = await LocalStorageService.getInstance();
  GetIt.I.registerSingleton<LocalStorageService>(localStorageService);
}
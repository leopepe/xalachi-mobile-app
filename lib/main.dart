import 'package:fowv/routes/config_route.dart';
import 'package:fowv/services/local_storage_service.dart';
import 'package:get_it/get_it.dart';

import 'package:flutter/material.dart';
import 'package:fowv/routes/webview_route.dart';
import 'services/service_locator.dart';
import 'package:fowv/utils/constants.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'To Be Defined',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          buttonTheme: ButtonThemeData(
            buttonColor: Colors.blue,
            textTheme: ButtonTextTheme.primary,
          )),
      home: _getStarterRoute(),
    );
  }

  Widget _getStarterRoute() {
    final String DBParam = GetIt.I<LocalStorageService>().get(DB_PARAM) ?? '';

    if (DBParam.isEmpty) {
      return ConfigRoute();
    } else {
      return WebViewRoute(initialURL: BASE_URL + DBParam);
    }
  }
}
